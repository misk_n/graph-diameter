//
// Created by Franck Thang on 29/05/2018.
//

#include "node.h"

Node::Node(size_t value, Node *left, Node *right): value_(value), left_(left), right_(right){

}

Node *Node::split() {
    Node *nw = this->left_;
    Node* r = nw->left_;
    nw->left_ = nw->right_;
    nw->right_ = nullptr;
    this->left_ = r;

    return nw;
}
