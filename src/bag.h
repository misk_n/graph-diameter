//
// Created by Franck Thang on 29/05/2018.
//

#ifndef TOKYO_UNIV_IMPLEMENTATION_BAG_H
#define TOKYO_UNIV_IMPLEMENTATION_BAG_H


#include "node.h"

class Bag {
public:
    explicit Bag(size_t size = 40);
    void bag_insert(Node*);
    bool is_empty();
    Bag* pennant_split();
    void bag_union(Bag*);

    size_t get_size();
    Node* get_pennant(size_t k);
private:
    const size_t size_;
    Node** array_;
};


#endif //TOKYO_UNIV_IMPLEMENTATION_BAG_H
