//
// Created by Franck Thang on 29/05/2018.
//

#ifndef TOKYO_UNIV_IMPLEMENTATION_TREE_H
#define TOKYO_UNIV_IMPLEMENTATION_TREE_H


#include <cstdio>

class Node {
public:
    Node(size_t value, Node *left, Node *right);
    Node* split();

    size_t value_;
    Node* left_;
    Node* right_;
};


#endif //TOKYO_UNIV_IMPLEMENTATION_TREE_H
