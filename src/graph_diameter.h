/*
 The MIT License (MIT)

 Copyright (c) 2015 Yuki Kawata

 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 the Software, and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __GRAPH_DIAMETER_H__
#define __GRAPH_DIAMETER_H__

#ifdef para
#define  _GLIBCXX_PARALLEL
#endif

#include <cstdio>
#include <cstdlib>
#include <vector>
#include <stack>
#include <algorithm>
#include <sys/time.h>

#include <fstream>
#include <sstream>
#include <iostream>

#ifdef para
#include <mpi.h>
#endif

#ifdef cpupara
#include "node.h"
#include "bag.h"

// Create this function to traverse the tree iteratively
void process_pennant_aux(Node *in_pennant,
                         Bag *out_bag,
                         const std::vector<std::vector<int> > &bfs_graph,
                         std::vector<int> &dist,
                         std::vector<int>& queue,
                         int& qt) {

    std::queue<Node*> queue_node;
    queue_node.push(in_pennant);

    while (!queue_node.empty()) {
        Node* pennant = queue_node.front();
        queue_node.pop();
        if (pennant->left_ != nullptr) queue_node.push(pennant->left_);
        if (pennant->right_ != nullptr) queue_node.push(pennant->right_);


        unsigned int nthread = bfs_graph[pennant->value_].size() > 200 ? 2 : 1;

        std::vector<Bag*> bag_reducer {out_bag};

        std::vector<int> counter_reducer{0};
        std::vector<std::vector<int>> node_reducer;
        std::vector<int> tmp(bfs_graph[pennant->value_].size());
        node_reducer.push_back(tmp);
        for (unsigned int i = 1; i < nthread; ++i) {
            bag_reducer.push_back(new Bag());
            counter_reducer.push_back(0);

            std::vector<int> tmp1(bfs_graph[pennant->value_].size());
            node_reducer.push_back(tmp1);
        }

        #pragma omp parallel for num_threads (nthread)
            for (size_t j = 0; j < bfs_graph[pennant->value_].size(); j++) {
                if (dist[bfs_graph[pennant->value_][j]] < 0) {
                    dist[bfs_graph[pennant->value_][j]] = dist[pennant->value_] + 1;

                    node_reducer[omp_get_thread_num()][counter_reducer[omp_get_thread_num()]++]
                            = bfs_graph[pennant->value_][j];

                    bag_reducer[omp_get_thread_num()]->bag_insert(
                            new Node((size_t) bfs_graph[pennant->value_][j], nullptr, nullptr));
                }
            }

        #pragma omp barrier
        for (int j = 0; j < counter_reducer[0]; ++j) {
            queue[qt++] = node_reducer[0][j];
        }

        for (unsigned int i = 1; i < nthread; ++i) {
            out_bag->bag_union(bag_reducer[i]);
            for (int j = 0; j < counter_reducer[i]; ++j) {
                queue[qt++] = node_reducer[i][j];
            }
        }
    }
}


void process_pennant(
        size_t in_pennant_size,
        Node *in_pennant,
        Bag *out_bag,
        const std::vector<std::vector<int> > &bfs_graph,
        std::vector<int> &dist,
        std::vector<int>& queue,
        int& qt
) {
    constexpr size_t grainsize = 7; // That means 2^7 in my case
    if (in_pennant_size < grainsize) {
        process_pennant_aux(in_pennant, out_bag, bfs_graph, dist, queue, qt);
    }

    else {
        Node* nw = in_pennant->split();
        // spawn
#pragma omp parallel sections
        {
#pragma omp section
            process_pennant(in_pennant_size / 2, nw, out_bag, bfs_graph, dist, queue, qt);
#pragma omp section
            process_pennant(in_pennant_size / 2, in_pennant, out_bag, bfs_graph, dist, queue, qt);
        }
#pragma omp barrier
        // sync
    }
}


void process_layer(
        Bag *in_bag,
        Bag *out_bag,
        const std::vector<std::vector<int> >& bfs_graph,
        std::vector<int>& dist,
        std::vector<int>& queue,
        int& qt)
{
    size_t bag_size = in_bag->get_size();

        for (size_t k = 0; k < bag_size; ++k) {
            Node* pennant = in_bag->get_pennant(k);

            if (pennant != nullptr) {
                process_pennant(k, pennant, out_bag, bfs_graph, dist, queue, qt);
            }

        }
}

void print_bag(Bag* in_bag) {
    size_t size = in_bag->get_size();
    std::cout << "Printing values ... \n";
    for (size_t k = 0; k < size; ++k) {
        if (in_bag->get_pennant(k) != nullptr) {
            Node* pennant = in_bag->get_pennant(k);

            std::queue<Node*> queue;
            queue.push(pennant);

            while (!queue.empty()) {
                Node* current_pennant = queue.front();
                queue.pop();
                if (current_pennant->left_ != nullptr) queue.push(current_pennant->left_);
                if (current_pennant->right_ != nullptr) queue.push(current_pennant->right_);

            }
        }
    }
}

long long int number_value(Bag* in_bag) {
    size_t size = in_bag->get_size();
    long long int ret = 0;
    for (size_t k = 0; k < size; ++k) {
        if (in_bag->get_pennant(k) != nullptr) {
            ret += pow(2, k);
        }
    }

    return ret;
}


#endif

class GraphDiameter {
    public:
    
    GraphDiameter() : V(0), diameter(0), numBFS(0), time(0) {}
    ~GraphDiameter() {}
    int GetDiameter(const std::vector <std::pair<int, int> > &edges, int num_double_sweep = numDefaultDoubleSweep);
    int GetDiameter(const char *filename, int num_double_sweep = numDefaultDoubleSweep);
    void PrintStatistics(void);
    
    private:
    
    static const int numDefaultDoubleSweep = 10;
    int V;
    int diameter;
    int numBFS;
    double time;
    std::vector <std::vector <int> > graph;
    std::vector <std::vector <int> > rgraph;

    #ifdef cpupara
    int bfs(int start,
        const std::vector<std::vector<int> >& bfs_graph,
        std::vector<int>& dist,
        std::vector<int>& queue);
    #endif
    
    double GetTime(void) {
        struct timeval tv;
        gettimeofday(&tv, NULL);
        return tv.tv_sec + tv.tv_usec * 1e-6;
    }
    
    int GetRandom(void) {
        #ifdef para
        int R;
        MPI_Comm_rank(MPI_COMM_WORLD, &R);
        static unsigned x = 123456789 + R;
        static unsigned y = 362436039 + R;
        static unsigned z = 521288629 + R;
        static unsigned w = 88675123 + R;
        #else
        static unsigned x = 123456789;
        static unsigned y = 362436039;
        static unsigned z = 521288629;
        static unsigned w = 88675123;
        #endif
        unsigned t;
        
        t = x ^ (x << 11);
        x = y;
        y = z;
        z = w;
        w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
        
        return w % V;
    }
};


#ifdef cpupara
int GraphDiameter::bfs(int start,
                       const std::vector<std::vector<int> >& bfs_graph,
                       std::vector<int>& dist,
                       std::vector<int>& queue)
{

    dist[start] = 0;

    Bag *bag = new Bag();
    int qt = 0;
    queue[qt++] = start;

    bag->bag_insert(new Node(start, nullptr, nullptr));

    while (!bag->is_empty())
    {
        auto *new_bag = new Bag();
        process_layer(bag, new_bag, bfs_graph, dist, queue, qt);
        bag = new_bag;
    }
    return qt;
}
#endif

int GraphDiameter::GetDiameter(const std::vector <std::pair<int, int> > &edges,
			       int num_double_sweep) {

    #ifdef para
    edges.size();
    int R;
    MPI_Comm_rank(MPI_COMM_WORLD, &R);
    #endif

    num_double_sweep = num_double_sweep;

    #ifndef para
    // Prepare the adjacency list
    {
        for (size_t i = 0; i < edges.size(); i++) {
            int from = edges[i].first;
            int to = edges[i].second;
            
            V = std::max(V, from + 1);
            V = std::max(V, to + 1);
        }
        
        graph.resize(V);
        rgraph.resize(V);
        
        for (size_t i = 0; i < edges.size(); i++) {
            int from = edges[i].first;
            int to = edges[i].second;
            
            graph[from].push_back(to);
            rgraph[to].push_back(from);
        }
    }
    #endif

    std::vector <int> scc(V);

    #ifdef para
    if (R == 1) {
    #endif
    
    // Decompose the graph into strongly connected components
    time = -GetTime();
    {
        int num_visit = 0, num_scc = 0;
        std::vector <int> ord(V, -1);
        std::vector <int> low(V);
        std::vector <bool> in(V, false);
        std::stack <int> s;
        std::stack <std::pair<int, int> > dfs;
        
        for (int i = 0; i < V; i++) {
            if (ord[i] != -1) continue;
            
            dfs.push(std::make_pair(i, -1));
            
            while (!dfs.empty()) {
                int v = dfs.top().first;
                int index = dfs.top().second;
                
                dfs.pop();
                
                if (index == -1) {
                    ord[v] = low[v] = num_visit++;
                    s.push(v);
                    in[v] = true;
                } else {
                    low[v] = std::min(low[v], low[graph[v][index]]);
                }
                
                for (index++; index < (int)graph[v].size(); index++) {
                    int w = graph[v][index];
                    
                    if (ord[w] == -1) {
                        dfs.push(std::make_pair(v, index));
                        dfs.push(std::make_pair(w, -1));
                        break;
                    } else if (in[w] == true) {
                        low[v] = std::min(low[v], ord[w]);
                    }
                }
                
                if (index == (int)graph[v].size() && low[v] == ord[v]) {
                    while (true) {
                        int w = s.top();
                        
                        s.pop();
                        in[w] = false;
                        scc[w] = num_scc;
                        
                        if (v == w) break;
                    }
                    
                    num_scc++;
                }
            }
        }
    }

    #ifdef para
    }
    #endif
    
    // Compute the diameter lower bound by the double sweep algorithm
    int qs, qt;
    std::vector <int> dist(V, -1);
    std::vector <int> queue(V);
    {
        #ifndef para
        for (int i = 0; i < num_double_sweep; i++) {
        #endif
            int start = GetRandom();
            std::cout << "start point: " << start << std::endl;

            #ifdef cpupara
            qt = bfs(start, graph, dist, queue);
            #else
            // forward BFS
            qs = qt = 0;
            dist[start] = 0;
            queue[qt++] = start;
            
            while (qs < qt) {
                int v = queue[qs++];
                
                for (size_t j = 0; j < graph[v].size(); j++) {
                    if (dist[graph[v][j]] < 0) {
                        dist[graph[v][j]] = dist[v] + 1;
                        queue[qt++] = graph[v][j];
                    }
                }
            }
            #endif

            #ifdef para
            # pragma omp parallel for
            #endif
            for (int j = 0; j < qt; j++) dist[queue[j]] = -1;

            #ifdef cpupara
            qt = bfs(queue[qt - 1], rgraph, dist, queue);
            #else
            // backward BFS
            start = queue[qt - 1];
            qs = qt = 0;
            dist[start] = 0;
            queue[qt++] = start;
            
            while (qs < qt) {
                int v = queue[qs++];
                
                for (size_t j = 0; j < rgraph[v].size(); j++) {
                    if (dist[rgraph[v][j]] < 0) {
                        dist[rgraph[v][j]] = dist[v] + 1;
                        queue[qt++] = rgraph[v][j];
                    }
                }
            }
            #endif

            #ifndef para
            diameter = std::max(diameter, dist[queue[qt - 1]]);
            #else
            int local_diameter = dist[queue[qt - 1]];
            std::cout << "local fs diameter: " << local_diameter << std::endl;
            MPI_Reduce(&local_diameter, &diameter, 1, MPI_INT, MPI_MAX, 0, MPI_COMM_WORLD);
            if (!R)
              std::cout << "global fs diameter: " << diameter << std::endl;
            #endif

            #ifdef para
            # pragma omp parallel for
            #endif
            for (int j = 0; j < qt; j++) dist[queue[j]] = -1;
        #ifndef para
        }
        #endif
    }

    #ifdef para
    if (R != 1)
      return -1;
    #endif
    
    // Order vertices
    std::vector <std::pair<long long, int> > order(V);
    {
        #ifdef para
        # pragma omp parallel for
        #endif
        for (int v = 0; v < V; v++) {
            int in = 0, out = 0;
            
            for (size_t i = 0; i < rgraph[v].size(); i++) {
                if (scc[rgraph[v][i]] == scc[v]) in++;
            }
            
            for (size_t i = 0; i < graph[v].size(); i++) {
                if (scc[graph[v][i]] == scc[v]) out++;
            }
            
            // SCC : reverse topological order
            // inside an SCC : decreasing order of the product of the indegree and outdegree for vertices in the same SCC
            order[v] = std::make_pair(((long long)scc[v] << 32) - in * out, v);
        }
        
        std::sort(order.begin(), order.end());
    }
    
    // Examine every vertex
    std::vector <int> ecc(V, V);
    {
        for (int i = 0; i < V; i++) {
            int u = order[i].second;
            
            if (ecc[u] <= diameter) continue;
            
            // Refine the eccentricity upper bound
            int ub = 0;
            std::vector <std::pair<int, int> > neighbors;
            
            for (size_t j = 0; j < graph[u].size(); j++) neighbors.push_back(std::make_pair(scc[graph[u][j]], ecc[graph[u][j]] + 1));
            
            sort(neighbors.begin(), neighbors.end());
            
            for (size_t j = 0; j < neighbors.size(); ) {
                int component = neighbors[j].first;
                int lb = V;
                
                for (; j < neighbors.size(); j++) {
                    if (neighbors[j].first != component) break;
                    lb = std::min(lb, neighbors[j].second);
                }
                
                ub = std::max(ub, lb);
                
                if (ub > diameter) break;
            }
            
            if (ub <= diameter) {
                ecc[u] = ub;
                continue;
            }
            
            // Conduct a BFS and update bounds
            numBFS++;
            qs = qt = 0;
            dist[u] = 0;
            queue[qt++] = u;
            
            while (qs < qt) {
                int v = queue[qs++];
                
                for (size_t j = 0; j < graph[v].size(); j++) {
                    if (dist[graph[v][j]] < 0) {
                        dist[graph[v][j]] = dist[v] + 1;
                        queue[qt++] = graph[v][j];
                    }
                }
            }
            
            ecc[u] = dist[queue[qt - 1]];
            diameter = std::max(diameter, ecc[u]);

            #ifdef para
            #pragma omp parallel for
            #endif
            for (int j = 0; j < qt; j++) dist[queue[j]] = -1;
            
            qs = qt = 0;
            dist[u] = 0;
            queue[qt++] = u;
            
            while (qs < qt) {
                int v = queue[qs++];
                
                ecc[v] = std::min(ecc[v], dist[v] + ecc[u]);
                
                for (size_t j = 0; j < rgraph[v].size(); j++) {
                    // only inside an SCC
                    if (dist[rgraph[v][j]] < 0 && scc[rgraph[v][j]] == scc[u]) {
                        dist[rgraph[v][j]] = dist[v] + 1;
                        queue[qt++] = rgraph[v][j];
                    }
                }
            }

            #ifdef para
            #pragma omp parallel for
            #endif
            for (int j = 0; j < qt; j++) dist[queue[j]] = -1;
        }
    }
    
    time += GetTime();
    
    return diameter;
}

int GraphDiameter::GetDiameter(const char *filename, int num_double_sweep) {

    std::vector<std::pair<int, int> > edges;

    #ifdef para
    edges.resize(100);
    int R;
    MPI_Comm_rank(MPI_COMM_WORLD, &R);
    std::ifstream file;
    std::string line;
    std::stringstream ss;
    std::string node1, node2;


    if (!R)
    {
      file.open(filename);
      if (!file.is_open()) {
        std::cerr << "Unable to open file: " << filename << std::endl;
        exit(2);
      }
      
      std::getline(file, line);
      V = static_cast<int>(std::stoi(line));
      for (int i = 0; i < V; ++i) {
        std::getline(file, line);
      }
    }

    MPI_Bcast(&V, 1, MPI_INT, 0, MPI_COMM_WORLD);
    graph.resize(V);
    rgraph.resize(V);

    int read = 1;
    while (read)
    {
      edges.clear();
      if (!R)
      {
        for (int i = 0; i < 100; i++)
        {
          if (!std::getline(file, line) || !(ss << line))
            break;
          ss >> node1;
          ss >> node2;
          ss.clear();
          int from = std::stoll(node1);
          int to = std::stoll(node2);
          edges.emplace_back(from, to);
        }
        read = edges.size();
      }
      MPI_Bcast(&read, 1, MPI_INT, 0, MPI_COMM_WORLD);
      if (R)
      {
        edges.resize(read);
      }
      MPI_Bcast(edges.data(), read * 2, MPI_INT, 0, MPI_COMM_WORLD);
      if (R)
      {
        for (int i = 0; i < read; i++) {
          int from = edges[i].first;
          int to = edges[i].second;            
          graph[from].push_back(to);
          rgraph[to].push_back(from);
        }
      }

      
    }

    return GetDiameter(edges, num_double_sweep);
    #else

    std::ifstream file(filename);
    if (!file.is_open()) {
        std::cerr << "Unable to open file: " << filename << std::endl;
        exit(2);
    }


    std::string line;
    std::getline(file, line);

    auto numberOfNode = static_cast<int>(std::stoi(line));

    for (int i = 0; i < numberOfNode; ++i) {
        std::getline(file, line);
    }

    std::stringstream ss;
    std::string node1, node2;

    while (std::getline(file, line) && (ss << line)) {

        ss >> node1;
        ss >> node2;
        ss.clear();
        int from = std::stoll(node1);
        int to = std::stoll(node2);
        edges.emplace_back(from, to);

    }

    file.close();    

    return GetDiameter(edges, num_double_sweep);
    #endif
}

void GraphDiameter::PrintStatistics(void) {
    printf("Diameter : %d\n", diameter);
    printf("#BFS : %d -> %d\n", V, numBFS);
    printf("Time : %lf sec\n", time);
}

#endif
