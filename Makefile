CXX = g++
CXXFLAGS = -O3 -Wall -Wextra

all : bin bin/test bin/paratest bin/bagparatest

bin :
	mkdir bin

bin/test : sample/test.cpp src/graph_diameter.h
	$(CXX) $(CXXFLAGS) -Isrc -o $@ $^

bin/paratest : sample/test.cpp src/graph_diameter.h
	mpic++ $(CXXFLAGS) -Isrc -Dpara -fopenmp -o $@ $^

bin/bagparatest : sample/test.cpp src/graph_diameter.h src/node.cpp src/bag.cpp
	mpic++ $(CXXFLAGS) -Isrc -Dpara -Dcpupara -fopenmp -o $@ $^

clean :
	rm -rf bin

.PHONY : all clean
