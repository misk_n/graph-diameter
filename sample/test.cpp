#include "graph_diameter.h"
#ifdef para
#include <mpi.h>
#endif

int main(int argc, char *argv[]) {

#ifdef para
    int N,me;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &N);
    MPI_Comm_rank(MPI_COMM_WORLD, &me);
    if (N < 3 || !(N % 2))
      {
	std::cerr << "Invali world size. Must be impair and superior to 2." << std::endl;
	return 0;
      }
#endif

    if (argc != 2) {
        fprintf(stderr, "Usage: test GRAPH\n");
        return 0;
    }
    
    GraphDiameter gd;
    int status = gd.GetDiameter(argv[1]);
    if (status != -1)
      gd.PrintStatistics();

#ifdef para
    MPI_Finalize();
#endif

    return 0;
}
