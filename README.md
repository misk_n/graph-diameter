Graph Diameter (parallel version)
=================================

Graph Diameter is a new algorithm for computing diameters of real directed graphs.
This version contains some experimentations to optimize the original algorithm.

Usage
-----

### From CUI Interface

    $ make
    $ bin/test [GRAPH]

* Execute `make` to build programs.
* Execute `test` to compute the diameter of a graph sequencially.
* Other executables are distributed and multithreaded implmentations

### From Your Program

    GraphDiameter gd;
    printf("%d\n", gd.GetDiameter(edge_list));

* Call `GetDiameter` to compute the diameter of a graph.

Please see `graph_diameter.h` and `test.cpp` for detailed information.

### Details

* This software use a matrix market format for graph file parsing.
https://math.nist.gov/MatrixMarket/

References
----------
* Takuya Akiba, Yoichi Iwata, and Yuki Kawata, **An Exact Algorithm for Diameters of Large Real Directed Graphs**. In SEA 2015.